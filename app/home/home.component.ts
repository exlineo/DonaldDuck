import { Component, OnInit } from "@angular/core";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls:[ "./home.component.css" ]
})
export class HomeComponent implements OnInit {
    titre:string = "Donald Duck";
    imageEntete:string = "https://cdn.pixabay.com/photo/2013/04/06/11/50/image-editing-101040_960_720.jpg";
    imageEnteteDPI = "res://logo";
    imageEnteteLocale="~/adresseduneImage.png";

    soustitre:string="Coin coin";
    informations:string="Viens découvrir le monde merveilleur de Kim et Donald";

    constructor() {
        // Use the component constructor to inject providers.
    }
    
    ngOnInit():void{
        // Init your component properties here.
    }
    // Ma méthode pour faire coin coin
    coincoin(){
        console.log("Dis coin coin !!!");
    }
}
