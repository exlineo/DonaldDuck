"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
        this.titre = "Donald Duck";
        this.imageEntete = "https://cdn.pixabay.com/photo/2013/04/06/11/50/image-editing-101040_960_720.jpg";
        this.imageEnteteDPI = "res://logo";
        this.imageEnteteLocale = "~/adresseduneImage.png";
        this.soustitre = "Coin coin";
        this.informations = "Viens découvrir le monde merveilleur de Kim et Donald";
        // Use the component constructor to inject providers.
    }
    HomeComponent.prototype.ngOnInit = function () {
        // Init your component properties here.
    };
    // Ma méthode pour faire coin coin
    HomeComponent.prototype.coincoin = function () {
        console.log("Dis coin coin !!!");
    };
    HomeComponent = __decorate([
        core_1.Component({
            selector: "Home",
            moduleId: module.id,
            templateUrl: "./home.component.html",
            styleUrls: ["./home.component.css"]
        }),
        __metadata("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());
exports.HomeComponent = HomeComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9tZS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJob21lLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLHNDQUFrRDtBQVFsRDtJQVNJO1FBUkEsVUFBSyxHQUFVLGFBQWEsQ0FBQztRQUM3QixnQkFBVyxHQUFVLGlGQUFpRixDQUFDO1FBQ3ZHLG1CQUFjLEdBQUcsWUFBWSxDQUFDO1FBQzlCLHNCQUFpQixHQUFDLHdCQUF3QixDQUFDO1FBRTNDLGNBQVMsR0FBUSxXQUFXLENBQUM7UUFDN0IsaUJBQVksR0FBUSx1REFBdUQsQ0FBQztRQUd4RSxxREFBcUQ7SUFDekQsQ0FBQztJQUVELGdDQUFRLEdBQVI7UUFDSSx1Q0FBdUM7SUFDM0MsQ0FBQztJQUNELGtDQUFrQztJQUNsQyxnQ0FBUSxHQUFSO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQ3JDLENBQUM7SUFuQlEsYUFBYTtRQU56QixnQkFBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLE1BQU07WUFDaEIsUUFBUSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ25CLFdBQVcsRUFBRSx1QkFBdUI7WUFDcEMsU0FBUyxFQUFDLENBQUUsc0JBQXNCLENBQUU7U0FDdkMsQ0FBQzs7T0FDVyxhQUFhLENBb0J6QjtJQUFELG9CQUFDO0NBQUEsQUFwQkQsSUFvQkM7QUFwQlksc0NBQWEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiBcIkhvbWVcIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vaG9tZS5jb21wb25lbnQuaHRtbFwiLFxuICAgIHN0eWxlVXJsczpbIFwiLi9ob21lLmNvbXBvbmVudC5jc3NcIiBdXG59KVxuZXhwb3J0IGNsYXNzIEhvbWVDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIHRpdHJlOnN0cmluZyA9IFwiRG9uYWxkIER1Y2tcIjtcbiAgICBpbWFnZUVudGV0ZTpzdHJpbmcgPSBcImh0dHBzOi8vY2RuLnBpeGFiYXkuY29tL3Bob3RvLzIwMTMvMDQvMDYvMTEvNTAvaW1hZ2UtZWRpdGluZy0xMDEwNDBfOTYwXzcyMC5qcGdcIjtcbiAgICBpbWFnZUVudGV0ZURQSSA9IFwicmVzOi8vbG9nb1wiO1xuICAgIGltYWdlRW50ZXRlTG9jYWxlPVwifi9hZHJlc3NlZHVuZUltYWdlLnBuZ1wiO1xuXG4gICAgc291c3RpdHJlOnN0cmluZz1cIkNvaW4gY29pblwiO1xuICAgIGluZm9ybWF0aW9uczpzdHJpbmc9XCJWaWVucyBkw6ljb3V2cmlyIGxlIG1vbmRlIG1lcnZlaWxsZXVyIGRlIEtpbSBldCBEb25hbGRcIjtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICAvLyBVc2UgdGhlIGNvbXBvbmVudCBjb25zdHJ1Y3RvciB0byBpbmplY3QgcHJvdmlkZXJzLlxuICAgIH1cbiAgICBcbiAgICBuZ09uSW5pdCgpOnZvaWR7XG4gICAgICAgIC8vIEluaXQgeW91ciBjb21wb25lbnQgcHJvcGVydGllcyBoZXJlLlxuICAgIH1cbiAgICAvLyBNYSBtw6l0aG9kZSBwb3VyIGZhaXJlIGNvaW4gY29pblxuICAgIGNvaW5jb2luKCl7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiRGlzIGNvaW4gY29pbiAhISFcIik7XG4gICAgfVxufVxuIl19